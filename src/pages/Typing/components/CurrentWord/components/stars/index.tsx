import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faStar, faStarHalfAlt, faStar as faStarRegular } from '@fortawesome/free-solid-svg-icons';

export type props = {
    rating: number
}
const StarRating = ({ rating } : props) => {
    const stars = [];

    // 创建星星的数量（例如，rating=3.5将创建3个整个的星星和1个半星）
    for (let i = 0; i < 5; i++) {
        if (i < Math.floor(rating)) {
            stars.push(<i key={i} className="fas fa-star text-yellow-500"></i>);
        } else if (i === Math.floor(rating) && rating % 1 !== 0) {
            stars.push(<i key={i} className="fas fa-star-half-alt text-yellow-500"></i>);
        } else {
            stars.push(<i key={i} className="far fa-star text-yellow-500"></i>);
        }
    }

    return <div className="flex items-center space-x-1">{stars}</div>;
};

export default StarRating;
