import {phoneticConfigAtom, wordRepeatConfigAtom} from '@/store'
import { Word } from '@/typings'
import { WordStat } from '@/typings'
import { useAtomValue } from 'jotai'
import Phonetic from './components/Phonetic'
import Translation from './components/Translation'
import Rating from "react-rating-stars-component";

import {default as WordComponent} from './components/Word'
import WordSound from "@/pages/Typing/components/CurrentWord/components/WordSound";

export type CurrentWordProps = {
  word: Word
  onFinish: (everWrong: boolean, wordStat: WordStat) => void
  isStart: boolean
  wordVisible: boolean
  readNum: Number
}

export default function CurrentWord({ readNum, word, onFinish, isStart, wordVisible }: CurrentWordProps) {
  const phoneticConfig = useAtomValue(phoneticConfigAtom)
/*    const wordRepeatConfig = useAtomValue(wordRepeatConfigAtom)

    let isVisiable = wordVisible;
    if (wordRepeatConfig.isRepeat && !wordVisible) {
        isVisiable = readNum < 2;
    }*/

    return (
        <div className="flex flex-col items-center">

            <WordComponent word={word.name} readNum={readNum}  onFinish={onFinish} isStart={isStart} wordVisible={wordVisible}/>
            {phoneticConfig.isOpen && <Phonetic word={word}/>}
            {/*当前翻译*/}
            <div className="flex items-center">
                <Rating
                    value={word.star}
                    count={5}
                    size={20}
                    edit={false}
                    isHalf={true}
                    activeColor="#ffd700"
                />
            </div>
            <Translation trans={word.trans.join('\n')}/>
            {/*例句*/}
            {wordVisible && word.examples && <div className="mt-4 max-w-2xl text-center border border-gray-300 p-4 rounded-lg">
                <span className="font-semibold">{word.examples}</span>
                <br/>
                <span className="text-gray-500">{word.examplesTrans}</span>
            </div>}
            {/*词性*/}
            {wordVisible && <div className="text-gray-500">
                <Translation trans={word.collins && word.collins.join('\n')}/>
            </div>}

        </div>
    )
}
